.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

PANDOC_VERSION = 2.11.0.4
SASS_VERSION = 1.24.2
pandoc = $(TOOLS)/pandoc-$(PANDOC_VERSION)/bin/pandoc
pandoc_options = --from=markdown-auto_identifiers-smart --wrap=preserve --variable=year:`date '+%Y'`
sass = $(TOOLS)/dart-sass/sass

content = $(patsubst content/%.md,/%,$(wildcard content/*.md))
$(info content: $(content))
static = $(patsubst static/%,/%,$(wildcard static/*))
images = $(patsubst images/%,/img/%,$(wildcard images/*))
paths = $(addsuffix .html,$(content)) \
    /css/main.css \
    /js/main.js \
    /js/plugins.js \
    /js/jquery-1.9.1.min.js \
    /js/modernizr-2.6.2.min.js \
    $(images) \
    $(static)

site: $(addprefix $(SITE),$(paths))

clean:
	rm -rf $(SITE) $(TOOLS)

$(pandoc):
	mkdir -p $(TOOLS)
	wget -O $(TOOLS)/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/$(PANDOC_VERSION)/pandoc-$(PANDOC_VERSION)-linux-amd64.tar.gz
	tar -xzf $(TOOLS)/pandoc.tar.gz -C $(TOOLS)

$(sass): | $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/$(SASS_VERSION)/dart-sass-$(SASS_VERSION)-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(addprefix $(SITE),$(static)): $(SITE)/%: static/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/index.html: content/index.md templates/index.html templates/header.html templates/footer.html | $(pandoc)
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/index.html --variable=nav-index --output=$@ $<

$(SITE)/404.html: content/404.md templates/404.html templates/header.html templates/footer.html | $(pandoc)
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/404.html --variable=nav-404 --output=$@ $<

$(SITE)/%.html: content/%.md templates/default.html templates/header.html templates/footer.html | $(pandoc)
	mkdir -p $(@D)
	$(pandoc) $(pandoc_options) --template=templates/default.html $(subst /, --variable=nav-,/$*) --output=$@ $<

$(SITE)/css/main.css: styles/main.scss styles/*.scss | $(SITE) $(sass)
	mkdir -p $(@D)
	$(sass) $< $@

$(SITE)/img/%: images/%
	mkdir -p $(@D)
	cp $< $@

$(SITE)/js/%: scripts/%
	mkdir -p $(@D)
	cp $< $@
