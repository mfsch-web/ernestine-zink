---
title: "Kontakt & Anreise"
description: "Die Praxis in Liestal BL ist in per Bahn, Bus und Auto gut zu erreichen."
sections:
  - title: "Kontaktangaben"
    slug: "kontaktangaben"
    contact: true
    name: "Ernestine Zink"
    description:
      - "Dipl. Mal- und Gestaltungstherapeutin ED"
      - "Diplomierte Psychologin FH"
      - "Supervisorin OdA ARTECURA"
    street: "Seltisbergerstrasse 5"
    zipcode: "4410"
    town: "Liestal"
    landline: "+41 (0)61 921 33 34"
    mobile: "+41 (0)79 626 56 22"
  - title: "Wie Sie mich finden"
    slug: "wie-sie-mich-finden"
    image: dsc0397-w800h450.jpg
    content: |
      Mit dem öffentlichen Verkehr: zehn Gehminuten vom Bahnhof Liestal entfernt, fünf Gehminuten von der Bushaltestelle Wasserturmplatz Liestal entfernt

      Mit dem Auto: Anfahrt bis Schulhaus Burg Liestal

      [Karte: Seltisbergerstr. 5, Liestal](http://maps.google.com/maps?q=Seltisbergerstrasse+5,+Liestal,+Switzerland "Google Maps")
---
