---
title: "Mal- & Gestaltungstherapie"
description: "Was ist Mal- und Gestaltungstherapie? Für wen ist sie geeignet? Wie wirkt sie?"
sections:
  - title: "Was verstehen wir unter Mal- und Gestaltungstherapie?"
    slug: "was-verstehen-wir-unter-mal-und-gestaltungstherapie"
    quote: "Jeder Mensch ist ein Künstler (Joseph Beuys)"
    content: |
      Dies ist eine Methode, bei der ich meine Erfahrungen, Gefühle, Stimmungen, meine Sehnsüchte und Wünsche in Farben und Formen und in verschiedenen Materialien ausdrücken kann.

      Die Symbolik des Gestalteten kann im gemeinsamen Gespräch erkundet und gewürdigt werden. Das gestaltete Bild, Objekt, Tanz oder Text ist „das Dritte“, das im Raum zwischen Ich und Du entstanden ist und es dient uns als Spiegel, in dem wir verborgene und schlummernde Seiten von uns entdecken können.

      Dadurch wird eine seelische Entwicklung angestossen, die uns wachsen und reifen lässt.

      Es ist also eine Möglichkeit, um seelische Prozesse mit Hilfe von Malen, Plastizieren, Körperausdruck, Ritualen und Gespräch sichtbar zu machen.

      Sie basiert auf der Tatsache, dass jeder Mensch in jedem Alter über ein schöpferisches Potential verfügt, das nur zu einem ganz kleinen Teil bewusst und genutzt ist, und das geweckt und entfaltet werden kann.

      Für dieses Gestalten braucht es keine Vorkenntnisse und speziellen Begabungen. Hilfreich ist die Neugier, sich mit sich und seiner Innenwelt auseinanderzusetzen: mit inneren Bildern, Empfindungen, Wünschen und Ängsten. So bekommen wir Zugang zu unseren ureigensten Kraftquellen und zur inneren Weisheit.
  - title: "Für wen eignet sich Mal- und Gestaltungstherapie?"
    slug: "fr-wen-eignet-sich-mal-und-gestaltungstherapie"
    image: pic-16-9-w800h450.jpg
    quote: "Warum bleibt ihr nicht bei euch selbst und greift in euren eigenen Schatz? Ihr tragt doch alle Wirklichkeit dem Wesen nach in euch. (Meister Ekkehart)"
    content: |
      Mal- und Gestaltungstherapie eignet sich allgemein für *Menschen, die neugierig sind, sich selber und ihr kreatives Potential besser kennenzulernen.*

      Andere Motivationen sind:

      - Ein inneres Bedürfnis nach Veränderung
      - Der Wunsch nach Standortbestimmung und Neuorientierung
      - Die Sehnsucht, die eigenen Ressourcen zu entdecken und im Alltag zu nutzen
      - Ein Anstoss von aussen wie Burnout, Beziehungskonflikte oder Erziehungsprobleme
      - Verlust eines nahen Menschen durch Tod, Verlust der Arbeit oder der körperlichen Gesundheit
      - Traumatische Erfahrungen wie Unfälle, Schockerlebnisse oder Übergriffe, auch sexuelle
      - Angst- und Panikerleben, Hoffnungslosigkeit und Depression
      - Fragen im Umgang mit Essen und dem eigenen Körper
  - title: "Wie wirkt die Maltherapie?"
    slug: "wie-wirkt-die-maltherapie"
    image: dsc0416-1-w800h450.jpg
    content: |
      Im Malen und Gestalten kann ich zu mir selber und zur Ruhe kommen. Ich habe die Möglichkeit, Stimmungen, Gefühle und belastende Erfahrungen mit verschiedenen Materialien (Papier, Farben, Tonerde, Stein, Holz oder mit Sprache)auszudrücken. So entsteht ein Prozess, der mich immer mehr zu mir führt, zu meinen Kraftquellen, aber auch zu meinen Verletzungen. Im achtsamen Gespräch im geschützten Rahmen *finde ich Worte für Dinge, die ich bis jetzt nur ahnungsweise wahrgenommen habe.*

      Es ist also eine Arbeitsweise, die auch noch *unbewusste Themen sichtbar machen kann.* Durch die künstlerische formale Gestaltung bekommen diese Themen einen Ausdruck und können im Bild verändert und bearbeitet werden. In dieser Gestaltung findet ein *Klärungsprozess statt, der das Ich stärkt,* so dass anstehende Aufgaben angepackt werden können. Dieses Tun wirkt wieder zurück auf das Unbewusste. So findet ein Austausch zwischen Innenwelt und Aussenwelt statt, zwischen bewussten Themen und dem, was noch in mir schlummert.

      *Dies alles schafft Boden und inneren Halt.*

      Der Prozess führt manchmal in die Vergangenheit, ist aber auch zukunftsorientiert und legt den Schwerpunkt auf das „Hier und Jetzt“. In all dem liegt die Möglichkeit einer Nachreifung.

      Diese Therapieform geht von der Zuversicht aus, dass es in jeder Lebensphase Veränderungsmöglichkeiten und Hoffnung gibt und dass ein nächster Schritt in eine gute Richtung möglich ist.
gallery:
  image1: dsc0425-1-w380h450.jpg
  image2: dsc0426-1-w380h450.jpg
  image3: dsc0428-2-w380h450.jpg
---
