---
title: "Atelier Ernestine Zink"
description: "Meine Angebote, meine Rolle als Therapeuting und Angaben zu meiner Person."
sections:
  - title: "Angebote"
    slug: "angebote"
    content: |
      - Einzeltherapie für Erwachsene und Kinder
      - Therapie für Paare
      - Lehrtherapie und Supervision

      ## Themenzentrierte Gruppen

      das innere Kind, die innere Kriegerin, Lehrerin, Heilerin, Visionärin, Märchen, lösungsorientiertes Malen u. a.

      **Aktuell**

      **EIN RAUM FÜR MICH IM SPANNUNGSFELD DES ALLTAGS**

      **Ausdrucksmalen für Frauen**

      In einer Gruppe von maximal fünf Frauen haben wir Gelegenheit, mit Farbe und Materialien zu experimentieren und damit einen Raum zu schaffen, in dem wir uns ausdrücken und spüren können. So verbinden wir uns mit unseren Ressourcen und schöpfen Kraft für den Alltag. Es braucht keinerlei Kenntnisse und Vorbildung. Eine gute Voraussetzung ist Neugier und die Lust, in einer spielerischen Art auf Spurensuche zu gehen und zu schauen, welche Ausdruckskraft in uns schlummert. Regelmässiges Malen hilft sich zu stabilisieren und das Vertrauen zu stärken in die eigene Handlungskompetenz.

      Daten: jeden Dienstag von 9.30 – 11 Uhr

      Ort: Malatelier an der Seltisbergerstr. 5, 4410 Liestal

      Kosten: Fr. 30.- pro Vormittag

      Anmeldung: Tel. 061 921 33 34 oder per mail: info@ernestine-zink.ch

      **Krankenkassen-Anerkennung über EMR EGK ASCA**
  - title: "Meine Rolle als Therapeutin"
    slug: "meine-rolle-als-therapeutin"
    image: dsc0430-w800h450.jpg
    content: |
      Als Therapeutin schaffe ich mit meinem Verhalten und mit meiner Ausstrahlung eine Atmosphäre, in der sich der Gestaltungsprozess entfalten kann. Mit meiner offenen, achtsamen Haltung mir selber und dem Du gegenüber gleite ich im Erlebnisstrom mit. Es geht also nicht um Behandlung im herkömmlichen Sinn. *Die Gestaltende lernt sich selber zu „behandeln“, indem sie ihr Leben selber in die Hand nimmt.*

      Ich als „Reisebegleiterin“ verstehe mich auch als Gegenüber, das sich zur Verfügung stellt für einen Dialog. *Dieser Dialog kann nährend sein,* indem ich Mut mache, stütze und bezeuge, Raum schaffe und den Raum halte, in dem etwas entstehen kann, das der Klientin Vertrauen schenkt. *Der Dialog kann aber auch spiegelnd sein* in dem Sinne, dass ich etwas, das ich wahrnehme, ausdrücke oder in einem gemeinsam gemalten Bild gestalte. Dies sind Momente des Klärens, Verstärkens, Verdeutlichens, des Gegenüberstellens im Dienste des Wesentlichen. Der gestaltende Mensch allein entscheidet, ob er auf diese Impulse eingehen möchte.
  - title: "Über mich"
    slug: "uber-mich"
    image: portrait-w800h450.jpg
    content: |
      ## Beruflicher Werdegang

      - Maturität/Primarlehrerinnenseminar
      - Mal- und Gestaltungstherapeutin ED
      - Diplomierte Psychologin IAP

      ## Weg und Erfahrungen

      Ich bin verheiratet und Mutter von zwei erwachsenen Kindern.

      *Als Mutter und Lehrerin* hatte ich das Glück, Menschen ins Leben zu begleiten und ihnen bei der Entwicklung von ihren Ressourcen zu helfen.

      *In der Erwachsenenbildung* kann ich durch Meditation und Achtsamkeit mit andern auf dem Weg sein auf der Suche nach Sinn und persönlichem inneren Wachstum.

      *Als Mal- und Kunsttherapeutin* begegne ich Menschen, die auf der Suche sind nach Wandlung, weil sie das möchten oder weil sie durch äussere Umstände dazu veranlasst wurden.

      *Als Psychologin* habe ich gelernt, diesen inneren Wandlungs- und Selbstheilungskräften zu vertrauen und daran zu glauben, dass es in jeder Situation möglich ist, den heilgebliebenen Kern trotz schweren traumatischen Erfahrungen wieder zu spüren. Diese Veränderungsprozesse kann man aber nicht machen, es braucht *Geduld, Hingabe* und das Warten auf den richtigen Zeitpunkt. Meine *ganzheitliche Arbeitsweise* ist dabei eine wichtige Unterstützung.

      *Mein Anliegen ist es, kreative, psychologische und spirituelle Ansätze zu verbinden.*

      Ich arbeite vor dem Hintergrund der analytischen Psychologie nach C.G. Jung, beziehe aber auch Elemente der Gestalttherapie und der Verhaltenstherapie mit ein.

      *Mitgliedschaften: SBAB /IAC /GPK*

      Regelmässige Fortbildungen sowie Supervision und Intervision gemäss den Richtlinien unseres Berufsverbandes sind für mich eine Selbstverständlichkeit.
---
