# TEAM

    Manuel Schmid - Concept, Design, Code - dergestalt.ch

# THANKS

    Walter Zink - Photography

# TECHNOLOGY COLOPHON

    HTML5, CSS3
    Normalize.css, jQuery, Modernizr
    GitLab, Netlify
    Google Fonts
