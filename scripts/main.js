/* Author: Manuel Schmid */

$(window).load(function() {

  // Activate automatic scrolling for local links
  $('#main nav').localScroll({duration:200});

  // Keep pagemenu in view
  function moveScroller() {
    var a = function() {
      var b = $(window).scrollTop();
      var d = $("#main").offset().top;
      var c=$("#local-nav");
      if (b>d) {
        //c.addClass("fixed");
        c.css("position","fixed");
      } else {
        if (b<=d) {
          //c.removeClass("fixed");
          c.css("position","absolute");
        }
      }
    };
    $(window).scroll(a);a()
  }
  if ($("#main").length > 0) {
    $(function() { moveScroller();});
  }

  // gallery function
  $('.gallery a').click(function(event) {
    event.preventDefault();
    var image = $(this).attr("href");
    $('#img-large').append('<img src="' + image + '" />').fadeIn(200);
  });
  $('#img-large').click(function(event) {
    $(this).fadeOut(200).empty();
  });


});
